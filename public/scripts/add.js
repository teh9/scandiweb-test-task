const cancelButton = $("#cancel-product-adding");
const saveButton = $("#save-product")

errors = {
    'emptyFields'  : 'Please, submit required data',
    'emptyDetails' : 'Please, provide the data of indicated type'
}

cancelButton.bind(
    'click',
    () => {
        window.location.href = '/';
    }
);

saveButton.bind(
    'click',
    () => {
        const form    = $("#product_form");
        const values  = $("#product_form input.formField");
        const type    = $("#productType").val();

        if(!dataParser(values, errors.emptyFields)){
            return false;
        }

        if( type === null ){
            showError('Please, submit required data');
            return false;
        }

        const details = $("#"+type+"Form > div > div > input");

        if(!dataParser(details, errors.emptyDetails)){
            return false;
        }

        form.submit();
});

dataParser = (array, errorCode) => {
    for ( let i = 0; i < array.length; i++ ) {
        if(array[i].value === ''){

            showError(errorCode);
            return false;
        }
    }

    return true;
}

showError = (errorText) => {
    $('#showError').html(errorText);
}

$('#productType').change( (e) => {
    $("#allForms > div").addClass("d-none");

    const form = $("#"+e.target.value+"Form");

    form.addClass("d-block");
    form.removeClass("d-none");
});