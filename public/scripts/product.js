const addButton = $("#add-product");

addButton.bind(
    'click',
    () => {
        window.location.href = 'add-product';
    }
);

const deleteButton = $("#delete-product-btn");

deleteButton.bind(
    'click',
    () => {
        $("#list_form").submit();
    }
)