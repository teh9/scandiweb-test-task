<?php

    namespace application\lib;

    class Validate {

        private $fields;

        private $uncheckedValues;

        public function __construct ($product) {

            $this->uncheckedValues = $product;

            $this->fields = [
                'sku'    => $this->validateSku(),
                'name'   => $this->validateName(),
                'price'  => $this->validatePrice(),
                'type'   => $this->validateType(),
                'params' => $this->validateParams(),
            ];
        }

        private function validateSku () {
            if(is_string($this->uncheckedValues['sku'])){
                return $this->uncheckedValues['sku'];
            }

            return false;
        }

        private function validateName () {
            if(is_string($this->uncheckedValues['name'])){
                return $this->uncheckedValues['name'];
            }

            return false;
        }

        private function validatePrice () {
            $price = $this->uncheckedValues['price'];

            if(isInt($price) || isFloat($price)){

                return $this->uncheckedValues['price'];
            }

            return false;
        }

        private function validateType () {
            if(is_string($this->uncheckedValues['type'])){
                return $this->uncheckedValues['type'];
            }

            return false;
        }

        public function validateParams () {

            $values = $this->uncheckedValues;

            $productAttributes = new ProductAttributes($values, $values['type']);

            return $productAttributes->getAttribute();
        }

        public function getValidatedFields (){

            foreach ($this->fields as $key => $value){
                if(!$value){
                    return false;
                }
            }

            return $this->fields;
        }

    }