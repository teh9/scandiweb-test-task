<?php

    namespace application\lib;

    class ProductAttributes {

        private $attribute;

        public function __construct ( $post, $type ) {

            $method = 'set'.ucfirst($type).'Attributes';

            $this->$method($post);
        }

        private function setDvdAttributes ( $values ) {

            if(isInt($values['size']) || isFloat($values['size'])) {
                $attributes = [
                    'size' => $values['size']
                ];

                $this->attribute = json_encode($attributes);

                return true;
            }

            return $this->attribute = false;
        }

        private function setFurnitureAttributes ( $values ) {

            $allowedAttributes = ['height', 'width', 'length'];

            $sortedKeysArray = array_intersect_key($values, array_flip($allowedAttributes));

            foreach ($sortedKeysArray as $key => $value){
                if(!isFloat($value) || !isInt($value)) {
                    return $this->attribute = false;
                }
            }

            $attributes = [
                'height' => $values['height'],
                'width'  => $values['width'],
                'length' => $values['length']
            ];

            $this->attribute = json_encode($attributes);

            return true;
        }

        private function setBookAttributes ( $values ) {

            if (isInt($values['weight']) || isFloat($values['weight'])) {

                $attributes = [
                    'weight' => $values['weight'],
                ];

                $this->attribute = json_encode($attributes);

                return true;
            }

            return $this->attribute = false;
        }

        public function getAttribute () {

            return $this->attribute;
        }

    }