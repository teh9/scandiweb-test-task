<?php

    namespace application\lib;

    class ProductList {

        public function prepareProductList ($list) {

            foreach ($list as $key => &$value) {

                $method = 'get'.ucfirst($value['type']).'Item';

                $value['params'] = $this->$method($value['params']);
            }

            return $list;
        }

        private function getDvdItem ( $item ) {

            $dvdSize = json_decode($item);

            $preparedDvd = 'Size: '.$dvdSize->size.' MB';

            return $preparedDvd;
        }

        private function getBookItem ( $item ) {

            $bookWeight = json_decode($item);

            $preparedBook = 'Weight: '.$bookWeight->weight.'KG';

            return $preparedBook;
        }

        private function getFurnitureItem ( $item ) {

            $furnitureDimension = json_decode($item);

            $dimension = [
                $furnitureDimension->height,
                $furnitureDimension->width,
                $furnitureDimension->length
            ];

            $preparedFurniture = 'Dimension: '.implode('x', $dimension);

            return $preparedFurniture;
        }

    }