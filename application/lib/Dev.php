<?php

    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    function pre( $array ){
        echo "<pre>";
            print_r( $array );
        echo "</pre>";
    }

    function isInt ( $value ) {

        if(filter_var($value, FILTER_VALIDATE_INT)){
            return true;
        }

        return false;
    }

    function isFloat ( $value ){

        if(filter_var($value, FILTER_VALIDATE_FLOAT)){
            return true;
        }

        return false;
    }