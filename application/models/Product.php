<?php

    namespace application\models;

    use application\core\Model;
    use application\lib\Validate;
    use application\lib\D;
    use application\lib\ProductList;

    class Product extends Model {

        private $hash = 'sc4nd1W3b'; # For SKU
        private $product;

        public function __construct()
        {
            parent::__construct();
        }

        public function productValidate ( $postValues ) {

            $this->product = new Validate($postValues);

            $checkedFields = $this->product->getValidatedFields();

            if($checkedFields){

                if($this->saveProduct($checkedFields)){
                    return true;
                }

                return false;
            }

            return false;
        }

        private function saveProduct ( $values ) {

            $product = D::load('products');

            $product->sku    = strtoupper($values['sku'].'-'.$this->makeSkuHash());
            $product->name   = $values['name'];
            $product->price  = $values['price'];
            $product->type   = $values['type'];
            $product->params = $values['params'];

            if(D::store($product)){
                return true;
            }

            return false;
        }

        public function deleteProduct ( $post ) {

            $itemsToDelete = D::load('products');

            if(D::trash($post)){
                return true;
            }

            return false;
        }

        public function getProductList () {

            $list = new ProductList();

            $items = D::findAll('products');

            return $list->prepareProductList($items);
        }

        private function makeSkuHash () {

            return substr(md5(time().$this->hash), -4);
        }
    }