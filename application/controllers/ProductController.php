<?php

    namespace application\controllers;

    use application\core\Controller;

    class ProductController extends Controller {

        public function indexAction() {

            if(!empty($_POST['deleteProduct'])){

                if($this->model->deleteProduct($_POST['deleteProduct'])){

                    $this->view->redirect('/');
                }
            }

            $vars = [
                'products' => $this->model->getProductList()
            ];

            $this->view->render('Product List', $vars);
        }

        public function addAction(){

            if(!empty($_POST)){
                if(!$this->model->productValidate($_POST)){

                    $this->view->redirect('/add-product');
                    return false;
                }

                $this->view->redirect('/');
                return true;
            }

            $this->view->render('Product Add');
        }

    }