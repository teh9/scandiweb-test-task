<?php

    namespace application\core;

    use application\lib\D;

    abstract class Model{

        public $db;

        public function __construct(){

            $this->db = D::setup('localhost', 'scandiweb', 'root', '');
        }

    }