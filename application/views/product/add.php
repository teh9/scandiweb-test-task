<div class="container mt-5">
    <div class="row">
        <div class="col-sm-8">
            <h1>Product Add</h1>
        </div>
        <div class="col-sm-4 d-flex align-items-center justify-content-end">
            <button class="btn btn-primary me-3" id="save-product">Save</button>
            <button class="btn btn-danger" id="cancel-product-adding">Cancel</button>
        </div>
    </div>
    <hr>
    <div>
        <p class="text-danger" id="showError"></p>
    </div>
    <div class="mt-4">
        <form method="post" id="product_form">
            <div class="container">
                <div class="row mb-3">
                    <div class="col-1">
                        <span>SKU</span>
                    </div>
                    <div class="col-11">
                        <input name="sku" class="formField" required id="sku" type="text">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-1">
                        <span>Name</span>
                    </div>
                    <div class="col-11">
                        <input name="name" class="formField" required id="name" type="text">
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-1">
                        <span>Price ($)</span>
                    </div>
                    <div class="col-11">
                        <input name="price" class="formField" required id="price" type="number">
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-2">
                        <span>Type Switcher</span>
                    </div>
                    <div class="col-10">
                        <select name="type" class="formField" required id="productType">
                            <option disabled selected>Select type</option>
                            <option value="dvd">DVD</option>
                            <option value="book">Book</option>
                            <option value="furniture">Furniture</option>
                        </select>
                    </div>
                </div>
                <div id="allForms">
                    <div id="dvdForm" class="d-none">
                        <div class="row mt-2">
                            <div class="col-2">
                                <span>Size (MB)</span>
                            </div>
                            <div class="col-10">
                                <input name="size" type="number" required id="size">
                            </div>
                        </div>
                        <p class="mt-4">Please, provide size</p>
                    </div>

                    <div id="furnitureForm" class="d-none">
                        <div class="row mt-2">
                            <div class="col-2">
                                <span>Height (CM)</span>
                            </div>
                            <div class="col-10">
                                <input name="height" type="number" required id="height">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-2">
                                <span>Width (CM)</span>
                            </div>
                            <div class="col-10">
                                <input name="width" type="number" required id="width">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-2">
                                <span>Length (CM)</span>
                            </div>
                            <div class="col-10">
                                <input name="length" type="number" required id="length">
                            </div>
                        </div>
                        <p class="mt-4">Please, provide dimension</p>
                    </div>

                    <div id="bookForm" class="d-none">
                        <div class="row mt-2">
                            <div class="col-2">
                                <span>Weight (KG)</span>
                            </div>
                            <div class="col-10">
                                <input name="weight" type="number" required id="weight">
                            </div>
                        </div>
                        <p class="mt-4">Please, provide weight</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>