<div class="container mt-5">
    <div class="row">
        <div class="col-sm-8">
            <h1>Product List</h1>
        </div>
        <div class="col-sm-4 d-flex align-items-center justify-content-end">
            <button class="btn btn-primary me-3" id="add-product">ADD</button>
            <button class="btn btn-danger" id="delete-product-btn">MASS DELETE</button>
        </div>
    </div>
    <hr>
    <form method="post" id="list_form">
        <div class="row">
            <?php foreach ($products as $key => $value) : ?>
            <div class="col-sm-3 mb-3">
                <div class="w-100 border">
                    <div class="mx-4 mb-1 mt-3">
                        <input
                                name="deleteProduct[]"
                                value="<?php echo $value['id']; ?>"
                                type="checkbox"
                                class="delete-checkbox"
                        >
                    </div>
                    <div class="d-flex flex-column align-items-center justify-content-end mb-3">
                        <p><?php echo $value['sku']; ?></p>
                        <p><?php echo $value['name']; ?></p>
                        <p><?php echo $value['price']; ?></p>
                        <p><?php echo $value['params']; ?></p>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </form>
</div>