<?php

    return [

        '' => [
            'controller' => 'product',
            'action'     => 'index'
        ],

        'add-product' => [
            'controller' => 'product',
            'action'     => 'add'
         ]

    ];